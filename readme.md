#Coding part#:

What has been done: 

1. Feature extraction

2. Classification with lda,qda.

3. PCA code(see comments in code)

TODO:

1. I kept the code in a public git repo, Kyle, should we go to a private git repo? In that case, put it in github and send us the link.

2. Logistic regression is not done yet.

3. Use different scores (I am using only accuracy now)

4. How many PCA dimensions? See code comments.

#Report part (https://www.writelatex.com/355163nyzdvj)#

What has been done:

1. Problem, dataset and feature extraction

TODO:

1. Generating different results (with different scores) and put them in the report.

2. Sagnik will probably add a few discussion points later.

3. Proof reading of the report.


#Presentation part#

Nothing has been done yet, but can use latex to make presentation. Will make life easier.
