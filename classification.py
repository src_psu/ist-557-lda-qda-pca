import numpy as np
import pickle
from sklearn.lda import LDA
from sklearn.qda import QDA
from sklearn.decomposition import PCA
from sklearn import cross_validation

def classify(traindata,target):
	print "lda"
	clf_lda = LDA()
	#do some cool 5 fold cross validation stuff here.
	#read http://scikit-learn.org/stable/modules/cross_validation.html
	#and http://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter
	#for further details about how scores can be changed.
	scores = cross_validation.cross_val_score(clf_lda, traindata, target, cv=5)
	print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
	
	print "qda"
	clf_qda  = QDA()
	scores = cross_validation.cross_val_score(clf_qda, traindata, target, cv=5)
	print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

	print "logistic regression"
	#TODO comment to Wenyi and Kyle: put your code here for logistic regression 


def main():
	print "loading training dataset features"
	traindata=np.asarray(pickle.load(open("traindata-allfeatures.list","r"))).astype(np.float)
	print "loading class labels for training dataset"
	target=np.asarray(pickle.load(open("target.list","r"))).astype(np.float)
	#results before PCA	
	classify(traindata,target)
	#results after PCA : TODO :  we have 14 features, how many dimensions should we finally keep?
	#should we plot a graph with x axis as varying number of dimensions and y axis as accuracy?
	pca = PCA(n_components='mle') #this will automatically detect number of dimensions, see documentation at 
	#http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA.fit_transform
	print "doing pca"
	transtraindata=pca.fit_transform(traindata)
	print "classification results after pca"
	classify(traindata,target)

if  __name__ =='__main__':
	main()
	

